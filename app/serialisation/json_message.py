# coding=utf-8
import arrow
import bot
import discord

__author__ = "Gareth Coles"

# Ignored: call, nonce, type
ATTRIBUTES = [
    "edited_timestamp", "timestamp", "tts", "content", "channel",
    "mention_everyone", "embeds", "id", "mentions", "author",
    "channel_mentions", "server", "attachments", "pinned",
    "role_mentions", "reactions"
]


class JSONMessage:
    attachments = None  #: List[Dict[str, str]]
    author = None  #: discord.Member
    channel = None  #: discord.Channel
    channel_mentions = None  #: List[discord.Channel]
    content = None  #: str
    edited_timestamp = None  #: Optional[datetime.datetime]
    embeds = None  #: List[discord.Embed]
    id = None  #: str
    mention_everyone = None  #: bool
    mentions = None  #: List[discord.Member]
    pinned = None  #: bool
    reactions = None  #: List[discord.Reaction]
    role_mentions = None  #: List[discord.Role]
    server = None  #: discord.Server
    timestamp = None  #: datetime.datetime
    tts = None  #: bool

    def __init__(self, **kwargs):
        for attr in ATTRIBUTES:
            setattr(self, attr, kwargs[attr])

    @staticmethod
    def from_message(message: discord.Message):
        attrs = {}

        for attr in ATTRIBUTES:
            attrs[attr] = getattr(message, attr)

        return JSONMessage(**attrs)

    @staticmethod
    def from_dict(d: dict, b: bot.Bot):
        d["channel_mentions"] = [
            b.get_channel(c) for c in d["channel_mentions"]
        ]

        if d["edited_timestamp"]:
            d["edited_timestamp"] = arrow.get(d["edited_timestamp"]).datetime

        d["embeds"] = []  # TODO

        server = b.get_server(d["server"])

        d["mentions"] = [server.get_member(m) for m in d["mentions"]]

        d["reactions"] = []  # TODO
        d["role_mentions"] = []  # TODO
        d["server"] = server
        d["timestamp"] = arrow.get(d["timestamp"]).datetime

        return JSONMessage(**d)

    def dict(self):
        return {
            "attachments": self.attachments,
            "author": self.author.id,
            "channel": self.channel.id,
            "channel_mentions": [c.id for c in self.channel_mentions],
            "content": self.content,
            "edited_timestamp":
                self.edited_timestamp.isoformat()
                if self.edited_timestamp
                else None,
            "embeds": [],  # TODO
            "id": self.id,
            "mention_everyone": self.mention_everyone,
            "mentions": [m.id for m in self.mentions],
            "pinned": self.pinned,
            "reactions": [],  # TODO
            "role_mentions": [r.id for r in self.role_mentions],
            "server": self.server.id,
            "timestamp": self.timestamp.isoformat(),
            "tts": self.tts
        }
