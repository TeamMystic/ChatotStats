ChatotStatus
============

This is an experimental project to explore the technical aspects and viability
of a bot that can archive every message on a server and provide interesting
statistics based on that data.

This may never make it to production. We'll see!

Licensing
---------

ChatotStatus is licensed under the Artistic License 2.0. You may read up on the license
in the [LICENSE file](https://gitlab.com/TeamMystic/ChatotStatus/blob/master/LICENSE),
or [at choosealicense.com](http://choosealicense.com/licenses/artistic-2.0/).

Setup
-----

1. Install Python 3.5 or later
2. Install requirements - `python3.5 -m pip install -r requirements.txt`
3. Copy `config.yml.example` to `config.yml` and fill it out
4. `python3.5 -m app`

Usage
-----

Users sending messages in the channels you specify in `config.yml` with the
permissions you define will be able to use the following commands:

* No command yet.
